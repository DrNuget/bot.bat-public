import dbl
import discord

import random
import asyncio

leaderboard=0

##config##

prefix="."
client=discord.Client()
token=""
#id of the owner and the channel to send level ups
owner=262199151645818880
update_channel=463287813681709056

#exp stores the total experience, lvl stores levels
#usage: lvl[userid] or exp[userid]
exp={}
lvl={}
#rank stores all the leaderboard participants in order by experience from highest to lowest
#usage: rank.append(userid) or rank[num]
rank=[]
#reactions[] stores rolenames by message id with the .react command, the contents should be left unaltered
#usage: if message.id in reactions: or reactions[message.id]
reactions={}
#ignored_channels stores all the channel ids added with .ignorech command
#usage: if message.channel.id in ignored_channels:
ignored_channels=[]
#the size on how many characters wide the experience bar is, the exp_bar() function does the rest
#usage: just change the number here
exp_bar_size=20

#lb_bool dictates when the leaderboard system is on and when it's not on
#usage: lb_bool=True/False
lb_bool=False

#the names of advertisement submission file and leaderboard file, the files act as a failsafe if the bot suddenly crashes
submission_file="submissions"
leaderboard_file="levels"

#basically just what the bot says when you run the .help command
helps=[
"bot.bat commands help :",
prefix+"lvl : your level",
prefix+"lb : show the top 5 leaderboard",
prefix+"submit (link) : submit a new advertisement"
]

#what emoji is used in reaction commands, i will later change this so the user can change it
react_emoji="\N{THUMBS UP SIGN}"

#what the bot says whenever someone levels up
lvlup="Congrats! <@{0.author.id}> is now at level %i!"

##config##

f=open(leaderboard_file, "r")

def exp_bar(userid, spacing):
	i=int((exp[userid]/( 5 * (lvl[userid]**2) + 50 * lvl[userid] + 100))*exp_bar_size)
	s="["
	for j in range(i):
		s+="#"
	for j in range(exp_bar_size-i):
		for k in range(spacing):
			s+=" "
		s+="."
	s+="]"
	return(s)

def owner_check(message):
	return(message.author.id==owner)

@client.event
async def on_ready():
	global leaderboard
	f=open(leaderboard_file, "r")
	for s in f.read().split("\n"):
		s=s.split(":")
		if not s==[''] and s[0][0]!="#":
			print(s)
			if not s[0]=="leaderboardid":
				rank.append(int(s[0]))
				lvl[rank[-1]]=int(s[1])
				exp[rank[-1]]=int(s[2])
			else:
				leaderboard=await client.get_channel(int(s[2])).fetch_message(int(s[1]))
				print(leaderboard.id)
	f.close()
	await client.change_presence(status=discord.Status.online, activity=discord.Game(prefix+"help"))
	print("Bot is ready.")
@client.event
async def on_raw_reaction_add(payload):
	if payload.message_id in reactions and payload.user_id!=client.user.id:
		member=client.get_guild(payload.guild_id).get_member(payload.user_id)
		channel=client.get_channel(payload.channel_id)
		msg=await channel.fetch_message(payload.message_id)
		await msg.remove_reaction(payload.emoji, member)
		if payload.emoji.name==react_emoji:
			await member.add_roles(discord.utils.get(msg.guild.roles,name=reactions[payload.message_id]))
@client.event
async def on_message(message):
	channel=message.channel
	global leaderboard, lb_bool
	if not message.channel.id in ignored_channels and lb_bool:
		if message.author.id in exp and message.author.id != client.user.id:
			exp[message.author.id]+=10
			for i in range(len(rank)):
				if rank[i]==message.author.id:
					if exp[message.author.id]>exp[rank[i-1]] and i>0:
						rank[i],rank[i-1]=rank[i-1],rank[i]
						break
			#same formula as mee6
			if exp[message.author.id]>=( 5 * (lvl[message.author.id]**2) + 50 * lvl[message.author.id] + 100):
				lvl[message.author.id]+=1
				await client.get_channel(update_channel).send( (lvlup.format(message) % lvl[message.author.id]) )
			if not leaderboard==0:
				s="```Leaderboards:\n"
				for i in range(len(rank)):
					s+=("{} {}\n  Level: %i\n  %s[{}/{}]\n".format(i+1, client.get_user(rank[i]).display_name, exp[rank[i]], ( 5 * (lvl[rank[i]]**2) + 50 * lvl[rank[i]] + 100)) % (lvl[rank[i]], exp_bar(rank[i], 0)))
				s+="```"
				await leaderboard.edit(content=s)
			f=open(leaderboard_file, "w")
			for i in rank:
				f.write("{}:{}:{}\n".format(i, lvl[i], exp[i]))
			if not leaderboard==0:
				f.write("leaderboardid:{}:{}".format(leaderboard.id, leaderboard.channel.id))
			f.close()
		else:
			if message.author.id != client.user.id:
				exp[message.author.id]=10
				lvl[message.author.id]=0
				rank.append(message.author.id)
		
	if message.content==prefix+"help":
		await channel.send("```"+"\n".join(helps)+"```")
		await message.delete()
	elif message.content==prefix+"lb":
		if len(rank)>=5:
			j=5
		else:
			j=len(rank)
		s="```Leaderboards (top {}):\n".format(j)
		for i in range(j):
			s+=("{} {} Level: %i Experience: \n %s [{}/{}]\n".format(i+1, client.get_user(rank[i]).display_name, exp[rank[i]], ( 5 * (lvl[rank[i]]**2) + 50 * lvl[rank[i]] + 100)) % (lvl[rank[i]], exp_bar(rank[i], 0)))
		s+="```"
		await channel.send(s)
	elif message.content==prefix+"setuplb":
		if owner_check(message):
			leaderboard=await channel.send("```Leaderboards:```")
			await message.delete()
	elif message.content==prefix+"startlb":
		lb_bool=True
	elif message.content==prefix+"stoplb":
		lb_bool=False
	elif message.content==prefix+"lvl":
		for i in range(len(rank)):
			if rank[i]==message.author.id:
				j=i+1
		await channel.send("```\n"+message.author.display_name+"\nRank: #{}\nLevel: {}\n".format(j,str(lvl[message.author.id]))+"{}[{}/{}]".format(exp_bar(message.author.id, 0),exp[message.author.id], ( 5 * (lvl[message.author.id]**2) + 50 * lvl[message.author.id] + 100))+"\n"+"```")
	elif message.content==prefix+"ignorech":
		ignored_channels.append(message.channel.id)
		await message.delete()
	elif message.content.startswith(prefix+"ban"):
		if owner_check(message) and not client.get_user(int(message.content.split(" ")[1][2:-1]))==None:
			await message.guild.get_member(int(message.content.split(" ")[1][2:-1])).add_roles(discord.utils.get(message.guild.roles, name="BANNED"))
			await message.delete()
	elif message.content.startswith(prefix+"unban"):
		if owner_check(message) and not client.get_user(int(message.content.split(" ")[1][2:-1]))==None:
			await message.guild.get_member(int(message.content.split(" ")[1][2:-1])).remove_roles(discord.utils.get(message.guild.roles, name="BANNED"))
			await message.delete()
	elif message.content.startswith(prefix+"submit"):
		f=open(submission_file, "a")
		f.write(message.content[1:])
		f.close()
	elif message.content.startswith(prefix+"react"):
		if owner_check(message):
			msg = (await channel.send(" ".join(message.content.split(" ")[1:-1])))
			reactions[msg.id]=message.content.split(" ")[-1]
			await message.delete()
			await msg.add_reaction(react_emoji)
		else:
			await channel.send("You do not have the permission for this command!")
	elif message.content==prefix+"bye":
		await message.delete()
		if owner_check(message):
			await client.close()
			quit()
		else:
			await channel.send("You do not have the permission for this command!")
client.run(token)
